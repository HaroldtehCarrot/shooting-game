﻿using UnityEngine;
using System.Collections;

public class ShooterScript : MonoBehaviour {
	public Transform bulletSource;
	public Rigidbody bulletTemplate;
	public float bulletSpeedX = -1000.0f;
	public float startTime = 0.0f;
	public float timeInterval = 2.0f;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire1")) { 	
			GenerateBall ();
		}
		if (Input.GetKey ("w")) { 	
			transform.position = transform.position + new Vector3 (0, 0.5f, 0);
		}
		if (Input.GetKey ("s")) { 	
			transform.position = transform.position + new Vector3 (0, -0.5f, 0);
		}
		if (Input.GetKey ("a")) { 	
			transform.position = transform.position + new Vector3 (0, 0, -0.5f);
		}
		if (Input.GetKey ("d")) { 	
			transform.position = transform.position + new Vector3 (0, 0, 0.5f);
		}
		if (Input.GetKey ("x")) { 	
			transform.position = transform.position + new Vector3 (0.5f, 0, 0);
		}
		if (Input.GetKey ("z")) { 	
			transform.position = transform.position + new Vector3 (-0.5f, 0, 0);
		}



	}
	void GenerateBall() {
		Vector3 pos = bulletSource.position; //start position to shoot bullet
		Quaternion rot = bulletSource.rotation; //starting rotation of bullet source
		Rigidbody generatedBall = (Rigidbody)Instantiate (bulletTemplate, pos, rot); //generate bullet
		generatedBall.AddForce (new Vector3 (bulletSpeedX, 0, 0)); //add speed to generated bullet so that it can move from right to left in a straight line
	}





}
