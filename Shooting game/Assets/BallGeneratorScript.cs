﻿using UnityEngine;
using System.Collections;

public class BallGeneratorScript : MonoBehaviour {
	public Rigidbody ballTemplate;

	public float ballSpeedX = 200.0f;
	public float ballSpeedY = -100.0f;
	public float startTime = 0.0f;
	public float timeInterval = 5.0f;

	// Use this for initialization
	void Start () {
		//here?
		InvokeRepeating ("GenerateBall", startTime, timeInterval);
	}
	
	// Update is called once per frame
	void Update () {
		//or here??

	}

	void GenerateBall() {
		Vector3 pos = transform.position;
		Quaternion rot = transform.rotation;
		Rigidbody generatedBall = (Rigidbody)Instantiate (ballTemplate, pos, rot);
		generatedBall.AddForce (new Vector3 (ballSpeedX, ballSpeedY, 0));
	}
}
